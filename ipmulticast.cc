
// Network topology
//
//                     Lan1
//                 ================
//                 |    |    |    | 
//       n0        n1   n2   n3   n4
//       |         |
//       ===========
//           Lan0
//
// - Multicast source is at node n0;
// - Multicast forwarded by node n1 onto LAN1;
// - Nodes n0, n1, n2, n3, and n4 receive the multicast frame.
//  ./waf --run="scratch/ipmulticast --MSize=2 --OverSub=4 --FileSize=100000000

#include <iostream>
#include <fstream>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/csma-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <sys/time.h>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("IpMulticast");

int 
main (int argc, char *argv[])
{
  //
  // Users may find it convenient to turn on explicit debugging
  // for selected modules; the below lines suggest how to do this
  //
  LogComponentEnable ("IpMulticast", LOG_LEVEL_INFO);
  //LogComponentEnable ("OnOffApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("PacketSink", LOG_LEVEL_INFO);
  //
  // Set up default values for the simulation.
  //
  // Select DIX/Ethernet II-style encapsulation (no LLC/Snap header)
  Config::SetDefault ("ns3::CsmaNetDevice::EncapsulationMode", StringValue ("Dix"));

  // Allow the user to override any of the defaults at
  // run-time, via command-line arguments
  uint16_t multicast_size = 5;
  uint32_t FileSize= 100000000;
  uint32_t OverSub= 1;	

  CommandLine cmd;
  cmd.AddValue ("MSize", "Multicast Group Size", multicast_size);
  cmd.AddValue ("FileSize", "Size of Multicast transmission", FileSize);
  cmd.AddValue ("OverSub", "Ratio of Over-subsctription", OverSub);

  cmd.Parse (argc, argv);
  
  double Data_Rates = 10000000000 / OverSub;

  NS_LOG_INFO ("Create nodes.");
   

  uint16_t i;

 // NodeContainer c;
 // c.Create (multicast_size + 2);
  
  // We will later want two subcontainers of these nodes, for the two LANs
  //NodeContainer c0 = NodeContainer (c.Get (0), c.Get (1));
  //NodeContainer c1 = NodeContainer (c.Get (1), c.Get (2), c.Get (3), c.Get (4), c.Get (5));


  NodeContainer c1;
  c1.Create (multicast_size + 1);

  Ptr<Node> c00 = CreateObject<Node> ();

  NodeContainer c0 = NodeContainer (c00, c1.Get (0));

  //for (i=0;i<multicast_size+1;i++){
  //    c1 = NodeContainer(c.Get (i+1));
 // }
  NS_LOG_INFO ("Build Topology.");
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (DataRate (Data_Rates)));
  csma.SetChannelAttribute ("Delay", StringValue ("5ns"));
 
  // We will use these NetDevice containers later, for IP addressing
  NetDeviceContainer nd0 = csma.Install (c0);  // First LAN
  NetDeviceContainer nd1 = csma.Install (c1);  // Second LAN

  NS_LOG_INFO ("Add IP Stack.");
  InternetStackHelper internet;
  internet.Install (c00);
  internet.Install (c1);

  NS_LOG_INFO ("Assign IP Addresses.");
  Ipv4AddressHelper ipv4Addr;
  ipv4Addr.SetBase ("10.1.1.0", "255.255.255.0");
  ipv4Addr.Assign (nd0);
  ipv4Addr.SetBase ("10.1.2.0", "255.255.255.0");
  ipv4Addr.Assign (nd1);

  NS_LOG_INFO ("Configure multicasting.");
  //
  // Now we can configure multicasting.  As described above, the multicast 
  // source is at node zero, which we assigned the IP address of 10.1.1.1 
  // earlier.  We need to define a multicast group to send packets to.  This
  // can be any multicast address from 224.0.0.0 through 239.255.255.255
  // (avoiding the reserved routing protocol addresses).
  //

  Ipv4Address multicastSource ("10.1.1.1");
  Ipv4Address multicastGroup ("225.1.2.4");

  // Now, we will set up multicast routing.  We need to do three things:
  // 1) Configure a (static) multicast route on node n2
  // 2) Set up a default multicast route on the sender n0 
  // 3) Have node n4 join the multicast group
  // We have a helper that can help us with static multicast
  Ipv4StaticRoutingHelper multicast;

  // 1) Configure a (static) multicast route on node n2 (multicastRouter)
  Ptr<Node> multicastRouter = c0.Get (1);  // The node in question
  Ptr<NetDevice> inputIf = nd0.Get (1);  // The input NetDevice
  NetDeviceContainer outputDevices;  // A container of output NetDevices
  outputDevices.Add (nd1.Get (0));  // (we only need one NetDevice here)

  multicast.AddMulticastRoute (multicastRouter, multicastSource, 
                               multicastGroup, inputIf, outputDevices);

  // 2) Set up a default multicast route on the sender n0 
  Ptr<Node> sender = c0.Get (0);
  Ptr<NetDevice> senderIf = nd0.Get (0);
  multicast.SetDefaultMulticastRoute (sender, senderIf);

  //
  // Create an OnOff application to send UDP datagrams from node zero to the
  // multicast group (node four will be listening).
  //
  NS_LOG_INFO ("Create Applications.");

  uint16_t multicastPort = 9;   // Discard port (RFC 863)

  // Configure a multicast packet generator that generates a packet
  // every few seconds
  OnOffHelper onoff ("ns3::UdpSocketFactory", 
                     Address (InetSocketAddress (multicastGroup, multicastPort))); //n1-N Receiver
  onoff.SetConstantRate (DataRate ("10Gb/s"));
  onoff.SetAttribute ("PacketSize", UintegerValue (5900));
  onoff.SetAttribute ("MaxBytes", UintegerValue (FileSize));

  ApplicationContainer srcC = onoff.Install (c0.Get (0)); //n1 the sender

  //
  // Tell the application when to start and stop.
  //
  srcC.Start (Seconds (1.));
  srcC.Stop (Seconds (10.));
///////////////////////////// Application Sink Loop///////////////

  for (i=0;i<multicast_size;i++){
      PacketSinkHelper sink ("ns3::UdpSocketFactory",
                         InetSocketAddress (Ipv4Address::GetAny (), multicastPort));  //n2-N Receiver

      ApplicationContainer sinkC = sink.Install (c1.Get (i+1));  //n2-N Receiver 
  // Start the sink
      sinkC.Start (Seconds (1.0));
      sinkC.Stop (Seconds (10.0));
  }
//////////////////////////////////////////////////////////////////

  NS_LOG_INFO ("Configure Tracing.");
  //
  // Configure tracing of all enqueue, dequeue, and NetDevice receive events.
  // Ascii trace output will be sent to the file "csma-multicast.tr"
  //
  //AsciiTraceHelper ascii;
  //csma.EnableAsciiAll (ascii.CreateFileStream ("IpMulticast.tr"));

  // Also configure some tcpdump traces; each interface will be traced.
  // The output files will be named:
  //     csma-multicast-<nodeId>-<interfaceId>.pcap
  // and can be read by the "tcpdump -r" command (use "-tt" option to
  // display timestamps correctly)
  //csma.EnablePcapAll ("IpMulticast", false);

  //
  // Now, do the actual simulation.
  //
  NS_LOG_INFO ("Run Simulation.");
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  Simulator::Run ();
  Simulator::Destroy ();

  gettimeofday(&tv2, NULL);
  printf("Execution Time = %f seconds\n",(double) (tv2.tv_usec - tv1.tv_usec) / 1000000 + (double) (tv2.tv_sec - tv1.tv_sec));
   
  NS_LOG_INFO ("Done.");
}
