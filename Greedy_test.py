# Solves IP given trafic matrix
def solve_Greedy(traf_matrix, splitters, hosts_occupied):

	jobs_scheduled = []
	
	for job in sorted(traf_matrix, key=lambda t: t['size'], reverse = True):
	
		if splitters == 0:
			break
		
		schedulable = True

		if hosts_occupied[job['src']] == True:
			schedulable = False

		if splitters < job['no_splitters']:
			schedulable = False

		for host in job['dst']:
			if hosts_occupied[host] == True:
				schedulable = False
		
		if schedulable == True:
			splitters = splitters - job['no_splitters']
			jobs_scheduled.append(job['id'])
			hosts_occupied[job['src']] = True
			for host in job['dst']:
				hosts_occupied[host] = True
			
	return jobs_scheduled
