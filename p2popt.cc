// Default Network topology, 9 nodes in a star
/*          (10.1.3.2)
(10.1.2.2)   n2 n3 n4   (10.1.4.2)
    	       \ | /
    	        \|/
(10.1.1.2) n1---n0---n5   (10.1.5.2)
    	        /| \
    	       / | \
(10.1.8.2)    n8 n7 n6    (10.1.6.2)
*/           
//     n1 multicasts data to n2-8 through multiple UDP unicasts.
//  ./waf --run="scratch/p2popt --MSize=2 --OverSub=4 --FileSize=100000000       
       
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/udp-socket.h"

#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <iostream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("P2pOpt");

int 
main (int argc, char *argv[])
{
  Time::SetResolution (Time::NS);
  // Users may find it convenient to turn on explicit debugging
  // for selected modules; the below lines suggest how to do this

  LogComponentEnable ("P2pOpt", LOG_LEVEL_INFO);
  //LogComponentEnable ("TcpL4Protocol", LOG_LEVEL_ALL);
  //LogComponentEnable ("TcpSocketImpl", LOG_LEVEL_ALL);
  LogComponentEnable ("PacketSink", LOG_LEVEL_INFO);
  LogComponentEnable ("OnOffApplication", LOG_LEVEL_INFO);

 

  // Allow the user to override any of the defaults and the above
  // Config::SetDefault()s at run-time, via command-line arguments

  uint16_t multicast_size = 5;
  uint32_t FileSize= 100000000;
  uint32_t OverSub= 1;	

  CommandLine cmd;
  cmd.AddValue ("MSize", "Multicast Group Size", multicast_size);
  cmd.AddValue ("FileSize", "Size of Multicast transmission", FileSize);
  cmd.AddValue ("OverSub", "Ratio of Over-subsctription", OverSub);

  cmd.Parse (argc, argv);
  
  double Data_Rates = 10000000000 / OverSub;
  uint32_t N = multicast_size + 2; //number of nodes in the star

  // Here, we will create N nodes in a star.
  NS_LOG_INFO ("Create nodes.");
  NodeContainer serverNode;
  NodeContainer clientNodes;
  serverNode.Create (1);
  clientNodes.Create (N-1);
  NodeContainer allNodes = NodeContainer (serverNode, clientNodes);

  // Install network stacks on the nodes
  InternetStackHelper internet;
  internet.Install (allNodes);

  //Collect an adjacency list of nodes for the p2p topology
  std::vector<NodeContainer> nodeAdjacencyList (N-1);
  for(uint32_t i=0; i<nodeAdjacencyList.size (); ++i)
    {
      nodeAdjacencyList[i] = NodeContainer (serverNode, clientNodes.Get (i));
    }

  // We create the channels first without any IP addressing information
  NS_LOG_INFO ("Create channels.");
  PointToPointHelper p2p;
 // p2p.SetDeviceAttribute ("DataRate", StringValue ("2.5Gbps"));
  p2p.SetDeviceAttribute ("DataRate", DataRateValue (DataRate (Data_Rates)));
  p2p.SetChannelAttribute ("Delay", StringValue ("2ns"));
  std::vector<NetDeviceContainer> deviceAdjacencyList (N-1);
  for(uint32_t i=0; i<deviceAdjacencyList.size (); ++i)
    {
      deviceAdjacencyList[i] = p2p.Install (nodeAdjacencyList[i]);
    }

  // Later, we add IP addresses.
  NS_LOG_INFO ("Assign IP Addresses.");
  Ipv4AddressHelper ipv4;
  std::vector<Ipv4InterfaceContainer> interfaceAdjacencyList (N-1);
  for(uint32_t i=0; i<interfaceAdjacencyList.size (); i++)
    {
      std::ostringstream subnet;
      subnet<<"10.1."<<i+1<<".0";
      ipv4.SetBase (subnet.str ().c_str (), "255.255.255.0");
      interfaceAdjacencyList[i] = ipv4.Assign (deviceAdjacencyList[i]);
    }
  
  //Turn on global static routing
  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
///////////// Loop Application //////////////////////
// sending UDP Packets from 10.1.1.2 n1 to 10.1.2.2 n2
  uint32_t servPort = 10000;
  
  for(uint16_t j=0; j<N-2; j++)
    {
      std::ostringstream subnet;

      subnet<<"10.1."<<j + 2<<".2";
      OnOffHelper oo = OnOffHelper("ns3::UdpSocketFactory",Address(InetSocketAddress (Ipv4Address(subnet.str ().c_str ()), servPort+j)));  //n2-N Receiver
 
      oo.SetAttribute ("PacketSize", UintegerValue (500)); //5900
      oo.SetAttribute ("DataRate", StringValue ("10Gbps"));
      oo.SetAttribute ("MaxBytes", UintegerValue (FileSize));

      ApplicationContainer serverApps = oo.Install (clientNodes.Get (0)); //n1 the sender
      serverApps.Start (Seconds (0.0+(20*j)));
      serverApps.Stop (Seconds (100.0));
          
      
      PacketSinkHelper sink ("ns3::UdpSocketFactory",Address(InetSocketAddress (Ipv4Address(subnet.str ().c_str ()), servPort+j))); //n2-N Receiver

      ApplicationContainer apps = sink.Install (clientNodes.Get (j+1)); //n2-N Receiver
      apps.Start (Seconds (0.0+(20*j)));
      apps.Stop (Seconds (100.0));
    }
//////////////////////////////
 // p2p.EnablePcapAll ("P2pOpt", false);

 // AsciiTraceHelper ascii;
 // p2p.EnableAsciiAll (ascii.CreateFileStream ("P2pOpt.tr"));
  ///////////////////////////////////////
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);
  
  NS_LOG_INFO ("Start Simulation.");
  Simulator::Run ();
  Simulator::Destroy ();
  
  gettimeofday(&tv2, NULL);
  printf("Execution Time = %f seconds\n",(double) (tv2.tv_usec - tv1.tv_usec) / 1000000 + (double) (tv2.tv_sec - tv1.tv_sec));
   
  return 0;
}
