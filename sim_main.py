# Test the resource allocation algorithms
import pulp
import random
import IPP_test_modified
import IPP_test_electrical
import Greedy_test
import time
import datetime as dt
import math
import subprocess
import string
import threading


time_now = dt.datetime.now()
f = open('Savefile'+ str(time_now),'w')

# Number of hosts
len_hosts = 100

# Test the optical network allocation if 1, electrical if 0
optical = 0

# Defining Simulation mode
# 1: Unicast, 2: IP Multicast, 3: Optical Multicast, 4: Point-to-point Optical Unicast
sim_mode = 1 # unicast



# Total number of splitters
num_splitters = 20

# IMPORTANT: For IP_test_electrical, set num_splitters = len_hosts 
if optical == 0:
	num_splitters = len_hosts

# Reconfigure after these many splitters become free while serving the scheduled jobs
# IMPORTANT: Splitters to wait for is equivalent to hosts_to_wait_for for IP_test_electrical
splitters_to_wait_for = 100

# Optical Link Speed = 10 Gbps or 1.25 GBps
link_speed = 1250000000.0

# Set 1 for solution via IP, default is greedy algorithm
algo_option = 1

# The number of output ports in a splitter, here we assume all splitters are identical
splitter_size = 4

# Total number of jobs
num_jobs = 20

# Multi-Threading function
def thread_call(w):
	p = subprocess.Popen(w, universal_newlines=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	text = p.stdout.read()
	#retcode = p.wait()
	#print text

# Variables to return
total_job_time = 0

hosts = range(len_hosts)

# Indicates which hosts are occupied by jobs
hosts_occupied = {}
for x in range (len(hosts)):
	hosts_occupied.update({x:False})


# Define and populate a random traffic matrix
traf_matrix = []

# Note: for IPP_test_electrical, use 'no_splitters' = number of source + destinations
if optical == 1:
	for x in range (0,num_jobs):
		rand = random.sample(range(len_hosts), random.randint(3,10))  # number of hosts
		rand_size = 250000000 * random.randint(1,10)
		#rand_size = random.randint(500000000,5000000000)  # Range of sizes in the traffic matrix
		temp = {'id':x}
		temp.update({'src' : rand[0]})
		temp.update({'dst' : rand[1:]})
		temp.update({'size' : rand_size})
		splitters_req = math.ceil((float(len(rand)-2))/(splitter_size-1))
		if splitters_req == 0:
			splitters_req = 1
		#+ math.ceil((float((len(rand)-1))/splitter_size) - math.floor((float(len(rand))-1)/splitter_size))
		temp.update({'no_splitters' : splitters_req})
		traf_matrix.append(temp)
else:
	for x in range (0,num_jobs):
		rand = random.sample(range(len_hosts), random.randint(3,9))  # number of hosts
		rand_size = 2500000 * random.randint(1,10)
		#rand_size = random.randint(200000000,2000000000)  # Range of sizes in the traffic matrix
		temp = {'id':x}
		src = rand[0]
		dst = rand[1:]
		temp.update({'src' : src})
		temp.update({'dst' : dst})
		temp.update({'size' : rand_size})
		splitters_req = 1+len(dst)
		temp.update({'no_splitters' : splitters_req})
		traf_matrix.append(temp)



'''

# Sample traffic matrices
if optical == 1:
	traf_matrix = [
		{"id": 0, "src": 0, "dst": [1,2],   "size": 8000000000, "no_splitters": 1},
		{"id": 1, "src": 3, "dst": [4,5], "size": 3000000000, "no_splitters": 1},
		{"id": 2, "src": 6, "dst": [3,7], "size": 5000000000, "no_splitters": 1},
	]
else:
	traf_matrix = [
		{"id": 0, "src": 9, "dst": [11,12],   "size": 10000000, "no_splitters": 3},
		{"id": 1, "src": 0, "dst": [4,5], "size": 10000000, "no_splitters": 3},
		{"id": 2, "src": 1, "dst": [3,7,8], "size": 10000000, "no_splitters": 4},
		{"id": 3, "src": 3, "dst": [11,12],   "size": 10000000, "no_splitters": 3},
		{"id": 4, "src": 6, "dst": [4,5], "size": 10000000, "no_splitters": 3},
		{"id": 5, "src": 3, "dst": [3,7,8], "size": 10000000, "no_splitters": 4},
	]
'''
print traf_matrix

f.write(str(traf_matrix)+"\n")

time1 = dt.datetime.now()

if optical == 1:
	if algo_option == 1:
		# Run the IP Allocation
		jobs_scheduled =(IPP_test_modified.solve_IP(traf_matrix, num_splitters, hosts, hosts_occupied))
	else:
		# Run the Greedy Allocation
		jobs_scheduled =(Greedy_test.solve_Greedy(traf_matrix, num_splitters, hosts_occupied))
else:
	if algo_option == 1:
		# Run the IP Allocation
		jobs_scheduled =(IPP_test_electrical.solve_IP(traf_matrix, num_splitters, hosts, hosts_occupied))
	else:
		# Run the Greedy Allocation
		jobs_scheduled =(Greedy_test_electrical.solve_Greedy(traf_matrix, num_splitters, hosts_occupied))


time2 = dt.datetime.now()
diff = time2 - time1
total_algorithm_time = diff
print "IP time :" + str(diff.seconds)+"."+str(diff.microseconds)
f.write('IP time :' + str(diff.seconds)+'.'+str(diff.microseconds)+'\n')
#print "IP time :" + str(diff.seconds)+"."+str(diff.microseconds)

while len(traf_matrix) > 0:
	
	cycle_time = 0
	elapsed_size = 0
	current_jobs = []
	jobs_carried_over = []
	traffic_carried_over = []
	print jobs_scheduled
	f.write(str(jobs_scheduled)+"\n")
	#print traf_matrix

	senders = []
	multicast_temp = []
	file_size = []

	# For storing the number of splitters being used for the current job configuration
	splitters_used = 0
	for job in jobs_scheduled:
		for request in traf_matrix:
			if job == request['id']:
				current_jobs.append(request)
				hosts_occupied[request['src']] = True
				for dest in request['dst']:
					hosts_occupied[dest] = True
	                        traf_matrix.remove(request)   # Remove job from traffic matrix
				splitters_used = splitters_used + request['no_splitters']
################## Start simulation running code #########################################################3
				temp = [];
				temp.append(request['size'])   	        
				file_size.extend(temp)
				temp = []
				temp.append(request['dst'])
			        multicast_temp.extend(temp)
				temp = []
				temp.append(request['src'])
			        senders.extend(temp)
					
	#print str(file_size)
	#print str(multicast_temp)
	time3 = dt.datetime.now()
	jobs =[]
	for i in range(len(file_size)):

		w = []
		if sim_mode == 1:
			temp1 = './waf --run=\"scratch/unicast ' 
			name = 'uni'
		elif sim_mode == 2:
			temp1 = './waf --run=\"scratch/ipmulticast ' 
			name = 'ipmult'
		elif sim_mode == 3:
			temp1 = './waf --run=\"scratch/optmulticast ' 
			name = 'optmult'
		elif sim_mode == 4:
			temp1 = './waf --run=\"scratch/p2popt ' 
			name = 'p2popt'

		temp2 = '--MSize='
		temp3 = str(len(multicast_temp[i]))
		temp4 = ' --OverSub=1'
		temp5 = ' --FileSize='
		temp6 = str(file_size[i])
		temp7 = '\"'
		temp8 = ' > '+name+'J'+str(jobs_scheduled[i])+'Tr'+str(senders[i])+'Rc'+str("-".join(str(x) for x in multicast_temp[i]))+'Si'+str(file_size[i])+'.out 2>&1'
		w = temp1+ temp2+ temp3+ temp4+ temp5+ temp6+ temp7 +temp8
		thread = threading.Thread(target=thread_call(w))
		thread.daemon = True
		thread.start()
		#jobs.append(thread)
		#w[i] = temp1+ temp2+ temp3+ temp4+ temp5+ temp6+ temp7 +temp8		
		#print w
		#p = subprocess.Popen(w, universal_newlines=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		#text = p.stdout.read()
		#retcode = p.wait()
		#print text
	#for j in jobs:
	#	j.start()
	#	print j

	#for j in jobs:
	#	j.join()
##################################### Finish Simulation Running Code #####################3


	# Splitters available before the scheduled jobs start servinig (note that an optimal configuration may not use all splitters)
	splitters_avail = num_splitters - splitters_used


	# Indicates if the optical switch is reconfigured before all the ongoing jobs are finished
	reconfigured = False # Initialize to false
	# Set of jobs carried over from the previous configuration
	jobs_carried_over = []
	
	for job in sorted(current_jobs, key=lambda t: t['size']):
		
		if reconfigured == False:
			
			# Additional amount of time taken to serve the next finishing job in line
			cycle_time = cycle_time + (job['size'] - elapsed_size)/link_speed
		
			# Increased the measure of elapsed_size to account for the current job
			elapsed_size = job['size']

			# The available splitters increase when a job finishes
			splitters_avail = splitters_avail + job['no_splitters']

			# Corresponding hosts also become free
			hosts_occupied[job['src']] = False
			for dest in job['dst']:
				hosts_occupied[dest] = False

			#print "Cycle time " + str(cycle_time)

			# Check if its time to reconfigure the switch
			if (splitters_avail >= splitters_to_wait_for or splitters_avail == num_splitters):  # Reconfigure the optical switch
				total_job_time = total_job_time + cycle_time		
				#print "Total time " + str(total_job_time)
				reconfigured = True

		# If the switch has been reconfigured then add the remaining jobs to be carried over to the next iteration
		else:
			job['size'] = job['size'] - elapsed_size
			if job['size'] == 0:
				splitters_avail = splitters_avail + job['no_splitters']
				hosts_occupied[request['src']] = False
				for dest in request['dst']:
					hosts_occupied[dest] = False
			
			elif job['size'] != 0:
				traffic_carried_over.append(job)
				jobs_carried_over.append(job['id'])
	
		
	if len(traf_matrix) != 0:

		time1 = dt.datetime.now()

		if optical == 1:
			if algo_option == 1:
				# Run the IP Allocation
				jobs_scheduled =(IPP_test_modified.solve_IP(traf_matrix, splitters_avail, hosts, hosts_occupied))
			else:
				# Run the Greedy Allocation
				jobs_scheduled =(Greedy_test.solve_Greedy(traf_matrix, splitters_avail, hosts_occupied))
		
		else:
			if algo_option == 1:
				# Run the IP Allocation
				jobs_scheduled =(IPP_test_electrical.solve_IP(traf_matrix, splitters_avail, hosts, hosts_occupied))
			else:
				# Run the Greedy Allocation
				jobs_scheduled =(Greedy_test_electrical.solve_Greedy(traf_matrix, splitters_avail, hosts_occupied))

	
		time2 = dt.datetime.now()
		diff = time2 - time1
		f.write('IP time :' + str(diff.seconds)+'.'+str(diff.microseconds)+'\n')
		total_algorithm_time = total_algorithm_time + diff
				
		traf_matrix.extend(traffic_carried_over)
		jobs_scheduled.extend(jobs_carried_over)

	elif len(traf_matrix) == 0 and len(jobs_carried_over) > 0:
		jobs_scheduled = []
		traf_matrix.extend(traffic_carried_over)
		jobs_scheduled.extend(jobs_carried_over)

				
print "Total job completion time: " + str(total_job_time)
print "Total algorithm time: " + str(total_algorithm_time)
f.write("Total job completion time: " + str(total_job_time)+"\n")
f.write("Total algorithm time: " + str(total_algorithm_time)+"\n")
#print str(jobs_scheduled)



