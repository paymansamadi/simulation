// Network topology
//
//                     Lan1
//                 ============================
//                 |    |    |    |    |    | 
//                 n0   n1   n2   n3   n4   n5
//                
//     n0 multicasts data to n1-5 through multiple UDP unicasts.
//
// Usage examples for things you might want to tweak:
//       ./waf --run="tcp-star-server"
//       ./waf --run="tcp-star-server --nNodes=25"
//       ./waf --run="tcp-star-server --ns3::OnOffApplication::DataRate=10000"
//       ./waf --run="tcp-star-server --ns3::OnOffApplication::PacketSize=500"
//
//  ./waf --run="scratch/unicast --MSize=2 --OverSub=4 --FileSize=100000000
       
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/csma-module.h"
#include "ns3/udp-socket.h"
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <iostream>
#include <sstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("unicast");

int
main (int argc, char *argv[])
{
  uint16_t multicast_size = 6;
  uint32_t FileSize= 100000000;
  uint32_t OverSub= 1;	

  CommandLine cmd;
  cmd.AddValue ("MSize", "Multicast Group Size", multicast_size);
  cmd.AddValue ("FileSize", "Size of Multicast transmission", FileSize);
  cmd.AddValue ("OverSub", "Ratio of Over-subsctription", OverSub);

  cmd.Parse (argc, argv);
  
  double Data_Rates = 10000000000 / OverSub;
  //std::ostringstream s;
  //s<<Data_Rates;
  //std::string channel_rate = s.str();
  //printf("%s\n", channel_rate);

  Time::SetResolution (Time::NS);
  LogComponentEnable ("unicast", LOG_LEVEL_INFO);
  LogComponentEnable ("OnOffApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("PacketSink", LOG_LEVEL_INFO);
  //LogComponentEnable("UdpL4Protocol", LOG_LEVEL_ALL);

  NodeContainer nodes;
  nodes.Create (multicast_size + 1);
  
  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", DataRateValue (DataRate (Data_Rates)));
  csma.SetChannelAttribute ("Delay", StringValue ("2ns"));

  NetDeviceContainer devices;
  devices = csma.Install (nodes);

  InternetStackHelper stack;
  stack.Install (nodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  Ipv4InterfaceContainer interfaces = address.Assign (devices);

///////////// Loop Application //////////////////////
  uint16_t servPort = 10000;

  uint16_t i;
  std::ostringstream subnet;

  for (i=0;i<multicast_size;i++){
      subnet<<"10.1.1."<<i + 2<<"";
      OnOffHelper oo = OnOffHelper("ns3::UdpSocketFactory",Address(InetSocketAddress (Ipv4Address(subnet.str ().c_str ()), servPort+i))); //n1-N Receiver
 
       oo.SetAttribute ("PacketSize", UintegerValue (5900)); //5900
       oo.SetAttribute ("DataRate", StringValue ("10Gbps"));
       oo.SetAttribute ("MaxBytes", UintegerValue (FileSize));

       ApplicationContainer serverApps = oo.Install (nodes.Get (0)); //n1 the sender
       serverApps.Start (Seconds (0.0+(20*i)));
       serverApps.Stop (Seconds (500.0));


       PacketSinkHelper sink ("ns3::UdpSocketFactory",Address(InetSocketAddress (Ipv4Address(subnet.str ().c_str ()), servPort+i)));  //n2-N Receiver

        ApplicationContainer apps = sink.Install (nodes.Get (i+1));  //n2-N Receiver
        apps.Start (Seconds (0.0+(20*i)));
        apps.Stop (Seconds (500.0));
        subnet.str(std::string());
  }

//////////////////////////////
  //csma.EnablePcapAll ("unicast", false);

  //AsciiTraceHelper ascii;
  //csma.EnableAsciiAll (ascii.CreateFileStream ("unicast.tr"));
  ///////////////////////////////////////
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);
  
  NS_LOG_INFO ("Start Simulation.");
  Simulator::Run ();
  Simulator::Destroy ();
  
  gettimeofday(&tv2, NULL);
  printf("Execution Time = %f seconds\n",(double) (tv2.tv_usec - tv1.tv_usec) / 1000000 + (double) (tv2.tv_sec - tv1.tv_sec));
   
  return 0;
}
