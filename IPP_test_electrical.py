# IP for solving the electrical network job allocation problem

import pulp


# Solves IP given trafic matrix
def solve_IP(traf_matrix, num_splitters, hosts, hosts_occupied):
# Activate a request

	# Generate the job-host incidence matrix
	m = [[0 for x in xrange(len(hosts))] for x in xrange(len(traf_matrix))]

	for index, request in enumerate(traf_matrix):
		for host_index,host in enumerate(hosts):
			if host in request['dst'] or host == request['src']:
				m[index][host_index] = 1

	a = pulp.LpVariable.matrix('activate', range(len(traf_matrix)),
	                            lowBound = 0,
	                            upBound = 1,
	                            cat = pulp.LpInteger)
	
	
	allocation_model = pulp.LpProblem("JobAllocation", pulp.LpMaximize)
	
	# Objective: maximize offloaded traffic: max \sum( a_i * s_i * d_i)
	allocation_model += sum([a[index] * request['size'] * len(request['dst']) for index, request in enumerate(traf_matrix)])
	
	# A host can only be allocated to one request
	for host_index, host in enumerate(hosts):	
			# Check if the host is occupied by a request scheduled and active from the previous iteration
			if hosts_occupied[host_index] == False:
				allocation_model += sum([a[index]*m[index][host_index] for index, request in enumerate(traf_matrix)]) <= 1, \
	        		                    "OneRequestAtSplitter"+str(host)
			else:	
				allocation_model += sum([a[index]*m[index][host_index] for index, request in enumerate(traf_matrix)]) <= 0, \
	        		                    "OneRequestAtSplitter"+str(host)
	


	# Solve the IP
	allocation_model.solve()
	
	# List of data to be returned
	jobs_scheduled = []
	
	for index,request in enumerate(traf_matrix):
	 	if a[index].value() == 1.0:
	        	jobs_scheduled.append(request['id'])
				
	return jobs_scheduled
